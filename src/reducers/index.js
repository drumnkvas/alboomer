import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import releases from './releases';

const rootReducer = combineReducers({
  releases,
  routing
});

export default rootReducer;
