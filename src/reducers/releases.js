const initialState = {
  query: '',
  releases: [],
  favorites: {}
};

export default function releases(state = initialState, action) {
  switch (action.type) {
    case 'SEARCH_SUCCESS_RELEASES':
      return { ...state, releases: action.releases };
    case 'SEARCH_QUERY_RELEASE':
      return { ...state, query: action.query, releases: action.query === '' ? [] : state.releases };
    case 'ADD_FAVORITE_RELEASE': {
      const favorites = { ...state.favorites };
      favorites[action.release.id] = action.release;
      return { ...state, favorites };
    }
    case 'REMOVE_FAVORITE_RELEASE': {
      const favorites = { ...state.favorites };
      delete favorites[action.release.id];
      return { ...state, favorites };
    }
    default:
      return state;
  }
}
