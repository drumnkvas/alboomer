import React from 'react';
import { Route, Switch } from 'react-router-dom';
import SearchPage from '../containers/SearchPage';
import FavoritesPage from '../containers/FavoritesPage';

export default (
  <Switch>
    <Route exact path="/" component={SearchPage} />
    <Route exact path="/favorites" component={FavoritesPage} />
  </Switch>
);
