import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styles from '../../src/style/header.scss';
import cssModules from 'react-css-modules';

@cssModules(styles)
export default class Header extends Component {
  render() {
    return (
      <div className={styles.header}>
        <h2>Alboomer</h2>
        <div className={styles.navigation}>
          <Link to="/">Search for Albums</Link>
          <Link to="/favorites">Favorites</Link>
        </div>
      </div>
    );
  }
}
