import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cssModules from 'react-css-modules';
import styles from '../style/release.scss';

import get from 'lodash/get';
import moment from 'moment';

const FontAwesome = require('react-fontawesome');

@cssModules(styles)
export default class Release extends Component {
  static propTypes = {
    release: PropTypes.object.isRequired,
    addToFavorites: PropTypes.func,
    removeFromFavorites: PropTypes.func,
    isFavorite: PropTypes.bool,
    styles: PropTypes.object
  };


  getReleaseDate = () => {
    const date = get(this.props.release, 'release-events[0].date', null);
    return date ? (<div>{moment(date, 'YYYY-MM-DD').format('YYYY')}</div>) : null;
  }


  getReleaseLabel = () => {
    const label = get(this.props.release, 'label-info[0].label.name', null);
    return label ? (<div>{label}</div>) : null;
  }


  render() {
    const { release, addToFavorites, removeFromFavorites, isFavorite } = this.props;

    return (
        <div className={styles.release} onClick={() => isFavorite ? removeFromFavorites(release) : addToFavorites(release)}>
          <div className={styles.favorite} >
            <FontAwesome
              name={isFavorite ? 'star' : 'star-o'}
              style={{ color: '#E8D59B' }}
              size="2x"/>
          </div>
          <div className={styles.info}>
            <div className={styles.title}>{release.title}</div>
            <div className={styles.artist}>{release['artist-credit'][0].artist.name}</div>
            <div className={styles.meta}>
              {this.getReleaseDate()}
              {this.getReleaseLabel()}
            </div>

          </div>
        </div>
    );
  }
}
