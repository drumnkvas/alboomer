import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cssModules from 'react-css-modules';
import styles from '../style/search.scss';

@cssModules(styles)
export default class Counter extends Component {
  static propTypes = {
    value: PropTypes.string,
    placeholder: PropTypes.string,
    onChange: PropTypes.func
  };

  render() {
    const { value, placeholder, onChange } = this.props;

    return (
      <div className={styles.bar}>
        <input type="text" value={value} onChange={(v) => onChange(v)} placeholder={placeholder}/>
      </div>
    );
  }
}
