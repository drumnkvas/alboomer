import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as ReleasesActions from '../../src/actions/releases';
import Search from '../components/Search';
import get from 'lodash/get';
import Release from '../components/Release';

class SearchPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      search: ''
    };
  }

  renderReleasesList = () => {
    const { releases, addToFavorites, removeFromFavorites, favorites } = this.props;
    const rs = get(releases, ['data', 'releases']);

    if (rs) {
      return (rs.map(r =>
        <Release release={r}
                 key={r.id}
                 addToFavorites={addToFavorites}
                 removeFromFavorites={removeFromFavorites}
                 isFavorite={!!favorites[r.id]}
        />
      ));
    }
    return null;
  }

  render() {
    return (
      <div>
        <Search
          placeholder="let's rock"
          value={this.props.query}
          onChange={e => {
            this.props.searchQuery(e.target.value);
            this.props.searchReleases(e.target.value);
          }}
        />
        <div>
          {this.renderReleasesList()}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) =>
  ({
    query: state.releases.query,
    releases: state.releases.releases,
    favorites: state.releases.favorites
  });

const mapDispatchToProps = (dispatch) =>
  ({
    searchQuery: query => dispatch(ReleasesActions.searchQuery(query)),
    searchReleases: query => dispatch(ReleasesActions.search(query)),
    addToFavorites: release => dispatch(ReleasesActions.addToFavorites(release)),
    removeFromFavorites: release => dispatch(ReleasesActions.removeFromFavorites(release)),
  });

export default connect(mapStateToProps, mapDispatchToProps)(SearchPage);
