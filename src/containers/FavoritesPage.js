import React, { Component } from 'react';
import * as ReleasesActions from '../../src/actions/releases';
import { connect } from 'react-redux';
import Release from '../components/Release';

class FavoritesPage extends Component {
  renderFavoritesList = () => {
    const { favorites, removeFromFavorites } = this.props;
    const favKeys = Object.keys(favorites);

    if (favKeys.length) {
      return (favKeys.map(k =>
        <Release release={favorites[k]}
                 key={favorites[k].id}
                 removeFromFavorites={removeFromFavorites}
                 isFavorite
        />
      ));
    } else {
      return <p>You have no favorites yet. Add some!</p>;
    }
  }

  render() {
    return (
      <div>
        <h3>Much loved albums of yours {"<"}3</h3>
        {this.renderFavoritesList()}
      </div>
    );
  }
}

const mapStateToProps = (state) =>
  ({
    favorites: state.releases.favorites
  });

const mapDispatchToProps = (dispatch) =>
  ({
    removeFromFavorites: release => dispatch(ReleasesActions.removeFromFavorites(release))
  });

export default connect(mapStateToProps, mapDispatchToProps)(FavoritesPage);
