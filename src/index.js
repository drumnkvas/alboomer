import React from 'react';
import { render } from 'react-dom';
import createHistory from 'history/createBrowserHistory';
import configureStore from './store/configureStore';
import Root from './containers/Root';

const store = configureStore().store;
const persistor = configureStore().persistor;
const history = createHistory();

render(
  <Root store={store} history={history} persistor={persistor} />,
  document.getElementById('root')
);
