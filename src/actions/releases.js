import { fetchReleases } from '../services/api';

export function searchSuccess(releases) {
  return { type: 'SEARCH_SUCCESS_RELEASES', releases };
}

export function searchQuery(query) {
  return { type: 'SEARCH_QUERY_RELEASE', query };
}

export function search(query) {
  return dispatch => fetchReleases(query).then(releases => {
    dispatch(searchSuccess(releases));
  }).catch(error => {
    throw (error);
  });
}

export function addToFavorites(release) {
  return { type: 'ADD_FAVORITE_RELEASE', release };
}

export function removeFromFavorites(release) {
  return { type: 'REMOVE_FAVORITE_RELEASE', release };
}
