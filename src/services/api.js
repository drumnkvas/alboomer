import axios from 'axios';

export const fetchReleases = (query) => axios.get(`http://musicbrainz.org/ws/2/release/?fmt=json&query=release:${query}`);
